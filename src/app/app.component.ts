import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OnAppStartProvider } from '../providers/on-app-start/on-app-start';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = "TabsPage";

  pages: Array<{title: string, component: any}>;

  constructor(private platform: Platform, 
              private statusBar: StatusBar, 
              private splashScreen: SplashScreen,              
              private onAppStart: OnAppStartProvider,
              

            ) {

    this.initializeApp();

    this.pages = [
                  {title: "Home", component: "TabsPage"},
                  //{title: "Settings", component: SettingsPage},
                  //{title: "Change Calculator", component: ChangeCalcPage},                  
                  {title: "Help", component: "HelpPage"},                  
                  {title: "About", component: "AboutPage"},
                  {title: "Credits", component: "CreditsPage"},

                ];
    
  }


  initializeApp(){
    this.platform.ready().then(() => {

      console.log("Platform is Ready");
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.statusBar.styleDefault();
        this.splashScreen.hide();

        
        
        this.onAppStart.onStart();
        
        

        
        
    });
  }
  
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
