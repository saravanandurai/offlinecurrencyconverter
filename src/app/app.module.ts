import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { CurrencyPipe } from '@angular/common';
import { IonicStorageModule } from '@ionic/storage';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ExRatesProvider } from '../providers/ex-rates/ex-rates';
import { SettingsProvider } from '../providers/settings/settings';
import { OnAppStartProvider } from '../providers/on-app-start/on-app-start';
import { BackgroundServiceProvider } from '../providers/background-service/background-service';

import { BackgroundMode } from '@ionic-native/background-mode';
import { AdMobFree } from '@ionic-native/admob-free';
import { Network } from '@ionic-native/network';
import { AdmobProvider } from '../providers/admob/admob';


@NgModule({
  declarations: [
    MyApp,    
  
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [   
    MyApp
    
   
  ],
  providers: [
    StatusBar,
    SplashScreen,    
    BackgroundMode,
    AdMobFree,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CurrencyPipe,
    ExRatesProvider,
    SettingsProvider,
    OnAppStartProvider,
    BackgroundServiceProvider,
    AdmobProvider,
   
        
  ]
})
export class AppModule {}
