import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HelpPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  expanded: Array<boolean> = [false, false, false]; 

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
             
            ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }
  ionViewWillEnter(){
  
  }

  expandItem(pos: number){
    
    this.expanded[pos] = !this.expanded[pos];
    
  }

}
