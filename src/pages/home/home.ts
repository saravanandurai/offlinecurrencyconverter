import { Component, NgZone } from '@angular/core';
import { NavController, IonicPage, Events } from 'ionic-angular';
import { CurrencyPipe } from '@angular/common';

import { CURRENCYCODES } from '../../data/currency-codes-data';
import { CurrencyCode } from '../../models/currency-code';

import { ExRatesProvider } from '../../providers/ex-rates/ex-rates';
import { SettingsProvider } from '../../providers/settings/settings';
import { OnAppStartProvider } from '../../providers/on-app-start/on-app-start';



@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  currencyCodes: CurrencyCode[] = CURRENCYCODES;
  payingCurr: string; 
  payingAmt: number = 1;
  payingCurrSymbol: string;
  payingCurrFlag: string;

  receivingCurr: string;
  receivingAmt: number = 0 ;  
  receivingCurrFlag: string;
  

  
  dataFlag: boolean = false;
  ratesFlag: boolean = false;

   


  constructor(  
              public navCtrl: NavController,
              private currencyPipe: CurrencyPipe,
              private exRates: ExRatesProvider,
              private settingsProv: SettingsProvider,
              private  onAppStart: OnAppStartProvider,
              private events: Events,
              private zone: NgZone,
              
             
              
              ) {

                

  }

  ionViewWillEnter(){
    console.log("ionViewWillEnter HomePage");
    this.settingsProv.getSettings().then(val=>{
                  if(val=="noData"){
                    console.log("HomePage Create App First Run Setting Not Set.");
                    this.events.subscribe("settingsSet",()=>{
                      this.zone.run(()=>{
                        this.settingsProv.getSettings().then(sVal=>{
                          this.payingCurr = sVal.homeCurr;
                          this.receivingCurr = sVal.frequentForeignCurr;
                          this.convert();                        
                        });                                            
                      })                      
                    })                    
                  }else{
                    this.payingCurr = val.homeCurr;
                    this.receivingCurr = val.frequentForeignCurr;
                    console.log("HomePage Create Settings Set");
                    this.convert();                   
                  }
                })
    this.events.subscribe("ratesUpdated",()=>{
      this.convert();
    })

  }

  
  convert(){

    //Currrency Symbol Paying Currency
      
    let payCurSyb : string = this.currencyPipe.transform(1,this.payingCurr,true,'1.2-2');
    
    if(payCurSyb.slice(0,(payCurSyb.length-4)) != this.payingCurr){
      this.payingCurrSymbol = payCurSyb.slice(0,(payCurSyb.length-4))
    }else{
      this.payingCurrSymbol = ""
    }
    console.log("curCode: " + payCurSyb.slice(0,(payCurSyb.length-4)));
    this.payingCurrFlag = "assets/images/flags/" + (this.currencyCodes.find(cCode=> cCode.code == this.payingCurr)).flag;
    this.receivingCurrFlag = "assets/images/flags/" + (this.currencyCodes.find(cCode=> cCode.code == this.receivingCurr)).flag;

    //=======================================
    let payRecCode: string = this.payingCurr + this.receivingCurr;
    console.log("payRecCode: " + payRecCode);

    this.exRates.getRates().then(rates=>{

      if(rates != null && rates != undefined && rates != "" && rates!= "noData"){
      let ratesObj = rates.quotes;//JSON.parse(JSON.stringify(RATES.quotes));
      let usdRecCurrRate = ratesObj["USD" + this.receivingCurr];
      let usdPayCurrRate = ratesObj["USD" + this.payingCurr];
      //console.log("Rates:" + "USD" + this.payingCurr + " : " + usdPayCurrRate);
      //console.log("Rates:" + "USD" + this.receivingCurr + " : " + usdRecCurrRate);
      this.receivingAmt = ((usdRecCurrRate/usdPayCurrRate)*this.payingAmt);    
      this.dataFlag = true;
      this.ratesFlag = false;
      }
    
      else{
        if(this.dataFlag == false)
          {
            this.ratesFlag = true;
          }
          //console.log("Rates vent ")
          this.events.subscribe("ratesSet",()=>{
            this.zone.run(()=>{
              this.convert(); 
            })          
          })           
      }   
    })
  }

  reverseCurrency(){
    var tempCurr: string;
    tempCurr = this.payingCurr;
    this.payingCurr = this.receivingCurr;
    this.receivingCurr = tempCurr;
    this.convert();
  }






}
