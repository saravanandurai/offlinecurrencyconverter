import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeCalcPage } from './change-calc';
@NgModule({
  declarations: [ChangeCalcPage],
  imports: [IonicPageModule.forChild(ChangeCalcPage)],
})
export class ChangeCalcPageModule { }