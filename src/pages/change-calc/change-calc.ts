import { Component, NgZone } from '@angular/core';
import { NavController, IonicPage, Events } from 'ionic-angular';
import { CurrencyPipe } from '@angular/common';

import { CURRENCYCODES } from '../../data/currency-codes-data';
import { CurrencyCode } from '../../models/currency-code';

import { ExRatesProvider } from '../../providers/ex-rates/ex-rates';
import { SettingsProvider } from '../../providers/settings/settings';


@IonicPage()
@Component({
  selector: 'page-change-calc',
  templateUrl: 'change-calc.html'
})
export class ChangeCalcPage {
  currencyCodes: CurrencyCode[] = CURRENCYCODES;
  payingCurr: string; 
  payingAmt: number = 1;
  receivingCurr: string;
  receivingAmt: number = 0 ;
  payingCurrSymbol: string;
  receivingCurrSymbol: string;
  costReceivingCurr: number = 0;
  payingCurrFlag: string;
  receivingCurrFlag: string;
  dataFlag: boolean = false;
  ratesFlag: boolean = false;

  constructor(
              public navCtrl: NavController,
              private currencyPipe: CurrencyPipe,
              private exRates: ExRatesProvider,
              private settingProv: SettingsProvider,
              private events: Events,
              private zone: NgZone,
              
            ) {
     

  }

  ionViewWillEnter(){
    this.settingProv.getSettings().then(val=>{
      
      this.payingCurr = val.homeCurr;
      this.receivingCurr = val.frequentForeignCurr;
      this.calculate();        
     });    
    this.events.subscribe("ratesUpdated", ()=>{
      this.calculate();
    })
                 
  }

  calculate(){

    //Currrency Symbol Paying Currency
      
    let payCurSyb : string = this.currencyPipe.transform(1,this.payingCurr,true,'1.2-2');
    
    if(payCurSyb.slice(0,(payCurSyb.length-4)) != this.payingCurr){
      this.payingCurrSymbol = payCurSyb.slice(0,(payCurSyb.length-4))
    }else{
      this.payingCurrSymbol = ""
    }
    console.log("PayCurCode: " + payCurSyb.slice(0,(payCurSyb.length-4)));
    

    //=======================================

    // Currrency Symbol Receiving Currency
    let recCurSym: string = this.currencyPipe.transform(1,this.receivingCurr,true,'1.2-2');
    if(recCurSym.slice(0,(recCurSym.length-4)) != this.receivingCurr){
      this.receivingCurrSymbol = recCurSym.slice(0,(recCurSym.length-4))
    }else{
      this.receivingCurrSymbol = ""
    }
    console.log("RecCurCode: " + recCurSym.slice(0,(recCurSym.length-4)));


    //======================
    // Setting Flags
    this.payingCurrFlag = "assets/images/flags/" + (this.currencyCodes.find(cCode=> cCode.code == this.payingCurr)).flag;
    this.receivingCurrFlag = "assets/images/flags/" + (this.currencyCodes.find(cCode=> cCode.code == this.receivingCurr)).flag;


    //======================================================
    let payRecCode: string = this.payingCurr + this.receivingCurr;
    //console.log("payRecCode: " + payRecCode);

    this.exRates.getRates().then(rates=>{
      if(rates != null && rates != undefined && rates != "" && rates!= "noData"){
        let ratesObj = rates.quotes;//JSON.parse(JSON.stringify(RATES.quotes));
        let usdRecCurrRate = ratesObj["USD" + this.receivingCurr];
        let usdPayCurrRate = ratesObj["USD" + this.payingCurr];
        //console.log("Rates:" + "USD" + this.payingCurr + " : " + usdPayCurrRate);
        //console.log("Rates:" + "USD" + this.receivingCurr + " : " + usdRecCurrRate);
        this.receivingAmt = ((usdRecCurrRate/usdPayCurrRate)*this.payingAmt) - this.costReceivingCurr;
        this.dataFlag = true;
        this.ratesFlag = false;
      }else{
        if(this.dataFlag == false)
          {
            this.ratesFlag = true;
          }
          this.events.subscribe("ratesSet",()=>{
            this.zone.run(()=>{
              this.calculate();
            })
            
          })

      }
    })  
  }


  reverseCurrency(){
    var tempCurr: string;
    tempCurr = this.payingCurr;
    this.payingCurr = this.receivingCurr;
    this.receivingCurr = tempCurr;
    this.calculate();
  }
}
