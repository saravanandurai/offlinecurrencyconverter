import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { CurrencyPipe } from '@angular/common';

import { SettingsProvider } from '../../providers/settings/settings'; 
import { Settings } from '../../models/settings';

import { CURRENCYCODES } from '../../data/currency-codes-data';
import { CurrencyCode } from '../../models/currency-code';
//import { AdmobProvider } from '../../providers/admob/admob';
import { BackgroundServiceProvider } from '../../providers/background-service/background-service';
import { ExRatesProvider } from '../../providers/ex-rates/ex-rates';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  currencyCodes: CurrencyCode[] = CURRENCYCODES;
  settings: Settings;
  dataFlag: boolean = false;
  homeCurrFlag: string;
  frequentForeignCurrFlag: string;
  homeCurrSymbol: string;
  frequentForeignCurrSymbol: string;
  lastUpdate: string;
  
  constructor(
              public navCtrl: NavController,
              private settingsProv: SettingsProvider,
              private currencyPipe: CurrencyPipe,
              private backgroundServ: BackgroundServiceProvider,
              //private admobProv: AdmobProvider,
              private exRates: ExRatesProvider,
              ) {
  }

  ionViewWillEnter(){
    /*
    
    this.admobProv.showBanner();
    this.admobProv.showInterstitial();
    this.admobProv.showVideo();
    */
  
    this.settingsProv.getSettings().then(val=>{      
        this.settings = val;        
        this.setFlagsAndSymbols();
        this.exRates.getLastUpdate().then(val=>{
          this.lastUpdate = val;
          this.dataFlag = true;
        });
        
      
      })
  }
    
    
  

  setCurrenciesAndRatesFlag(){    
    this.settingsProv.setSettings(this.settings).then(val=>{          
      this.setFlagsAndSymbols();
    })
  }

  setFlagsAndSymbols(){
    this.homeCurrFlag = "assets/images/flags/" + (this.currencyCodes.find(cCode=> cCode.code == this.settings.homeCurr)).flag;
    this.frequentForeignCurrFlag = "assets/images/flags/" + (this.currencyCodes.find(cCode=> cCode.code == this.settings.frequentForeignCurr)).flag;
    this.homeCurrSymbol = this.getSymbol(this.settings.homeCurr);
    this.frequentForeignCurrSymbol = this.getSymbol(this.settings.frequentForeignCurr);
     
  }

  getSymbol(cCode: string):string{
    let curSym : string = this.currencyPipe.transform(1,cCode,true,'1.2-2');    
    if(curSym.slice(0,(curSym.length-4)) != cCode){
      return curSym.slice(0,(curSym.length-4));      
    }else{
      return "";
    }  
  }

  setGetRatesFlag(){
    this.setCurrenciesAndRatesFlag();
    if(this.settings.ratesFetch == false){
      console.log("False Get Data");
      this.backgroundServ.stopBackgroundGetRates();
      
    }
    if(this.settings.ratesFetch == true){
      console.log("True Get Data");
      
      this.backgroundServ.setBackgroundGetRates();
    }  
  }
}
