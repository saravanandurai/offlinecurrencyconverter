import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { AdmobProvider } from '../../providers/admob/admob';


@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'HomePage';
  tab2Root = 'ChangeCalcPage';
  tab3Root = 'SettingsPage';

  constructor(
              private admobProv: AdmobProvider,
              private events: Events,
  ) {

  }

  ionViewWillEnter(){
    //this.admobProv.showBanner();
    //this.admobProv.showInterstitial();
    console.log("IonViewWillEnter TabsPage");
    /*
    this.events.subscribe("runVideoAd",()=>{
      this.admobProv.showVideo();
    })
    */
    
    
  }

  ionViewWillLeave(){
    console.log("IonViewWillLeave TabsPage");
    //this.events.unsubscribe("runVideoAd");
  }
}
