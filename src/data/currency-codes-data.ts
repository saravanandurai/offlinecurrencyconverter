import { CurrencyCode } from "../models/currency-code";

export const CURRENCYCODES: CurrencyCode[]  = [

    {"code":"AED","country":"United Arab Emirates Dirham","flag":"151-united-arab-emirates.png"},
{"code":"AFN","country":"Afghan Afghani","flag":"111-afghanistan.png"},
{"code":"ALL","country":"Albanian Lek","flag":"099-albania.png"},
{"code":"AMD","country":"Armenian Dram","flag":"108-armenia.png"},
{"code":"ANG","country":"Netherlands Antillean Guilder","flag":"237-netherlands.png"},
{"code":"AOA","country":"Angolan Kwanza","flag":"117-angola.png"},
{"code":"ARS","country":"Argentine Peso","flag":"198-argentina.png"},
{"code":"AUD","country":"Australian Dollar","flag":"234-australia.png"},
{"code":"AWG","country":"Aruban Florin","flag":"042-aruba.png"},
{"code":"AZN","country":"Azerbaijani Manat","flag":"141-azerbaijan.png"},
{"code":"BAM","country":"Bosnia-Herzegovina Convertible Mark","flag":"132-bosnia-and-herzegovina.png"},
{"code":"BBD","country":"Barbadian Dollar","flag":"084-barbados.png"},
{"code":"BDT","country":"Bangladeshi Taka","flag":"147-bangladesh.png"},
{"code":"BGN","country":"Bulgarian Lev","flag":"168-bulgaria.png"},
{"code":"BHD","country":"Bahraini Dinar","flag":"138-bahrain.png"},
{"code":"BIF","country":"Burundian Franc","flag":"057-burundi.png"},
{"code":"BMD","country":"Bermudan Dollar","flag":"081-bermuda.png"},
{"code":"BND","country":"Brunei Dollar","flag":"119-brunei.png"},
{"code":"BOB","country":"Bolivian Boliviano","flag":"150-bolivia.png"},
{"code":"BRL","country":"Brazilian Real","flag":"255-brazil.png"},
{"code":"BSD","country":"Bahamian Dollar","flag":"120-bahamas.png"},
{"code":"BTC","country":"Bitcoin","flag":"no-flag.png"},
{"code":"BTN","country":"Bhutanese Ngultrum","flag":"040-bhutan.png"},
{"code":"BWP","country":"Botswanan Pula","flag":"126-botswana.png"},
{"code":"BYN","country":"Belarusian Ruble","flag":"135-belarus.png"},
{"code":"BYR","country":"Belarusian Ruble","flag":"135-belarus.png"},
{"code":"BZD","country":"Belize Dollar","flag":"078-belize.png"},
{"code":"CAD","country":"Canadian Dollar","flag":"243-canada.png"},
{"code":"CDF","country":"Congolese Franc","flag":"249-democratic-republic-of-congo.png"},
{"code":"CHF","country":"Swiss Franc","flag":"205-switzerland.png"},
{"code":"CLF","country":"Chilean Unit of Account (UF)","flag":"131-chile.png"},
{"code":"CLP","country":"Chilean Peso","flag":"131-chile.png"},
{"code":"CNY","country":"Chinese Yuan","flag":"034-china.png"},
{"code":"COP","country":"Colombian Peso","flag":"177-colombia.png"},
{"code":"CRC","country":"Costa Rican Colón","flag":"156-costa-rica.png"},
{"code":"CUC","country":"Cuba Convertible Peso","flag":"153-cuba.png"},
{"code":"CUP","country":"Cuban Peso","flag":"153-cuba.png"},
{"code":"CVE","country":"Cape Verdean Escudo","flag":"038-cape-verde.png"},
{"code":"CZK","country":"Czech Republic Koruna","flag":"149-czech-republic.png"},
{"code":"DJF","country":"Djiboutian Franc","flag":"068-djibouti.png"},
{"code":"DKK","country":"Danish Krone","flag":"174-denmark.png"},
{"code":"DOP","country":"Dominican Peso","flag":"186-dominica.png"},
{"code":"DZD","country":"Algerian Dinar","flag":"144-algeria.png"},
{"code":"EEK","country":"Estonian Kroon","flag":"008-estonia.png"},
{"code":"EGP","country":"Egyptian Pound","flag":"158-egypt.png"},
{"code":"ERN","country":"Eritrean Nakfa","flag":"065-eritrea.png"},
{"code":"ETB","country":"Ethiopian Birr","flag":"005-ethiopia.png"},
{"code":"EUR","country":"Euro","flag":"259-european-union.png"},
{"code":"FJD","country":"Fijian Dollar","flag":"137-fiji.png"},
{"code":"FKP","country":"Falkland Islands Pound","flag":"215-falkland-islands.png"},
{"code":"GBP","country":"British Pound Sterling","flag":"260-united-kingdom.png"},
{"code":"GEL","country":"Georgian Lari","flag":"256-georgia.png"},
{"code":"GGP","country":"Guernsey Pound","flag":"204-guernsey.png"},
{"code":"GHS","country":"Ghanaian Cedi","flag":"053-ghana.png"},
{"code":"GIP","country":"Gibraltar Pound","flag":"213-gibraltar.png"},
{"code":"GMD","country":"Gambian Dalasi","flag":"146-gambia.png"},
{"code":"GNF","country":"Guinean Franc","flag":"110-guinea.png"},
{"code":"GTQ","country":"Guatemalan Quetzal","flag":"098-guatemala.png"},
{"code":"GYD","country":"Guyanaese Dollar","flag":"my-own-guyana.png"},
{"code":"HKD","country":"Hong Kong Dollar","flag":"183-hong-kong.png"},
{"code":"HNL","country":"Honduran Lempira","flag":"024-honduras.png"},
{"code":"HRK","country":"Croatian Kuna","flag":"164-croatia.png"},
{"code":"HTG","country":"Haitian Gourde","flag":"185-haiti.png"},
{"code":"HUF","country":"Hungarian Forint","flag":"115-hungary.png"},
{"code":"IDR","country":"Indonesian Rupiah","flag":"209-indonesia.png"},
{"code":"ILS","country":"Israeli New Sheqel","flag":"155-israel.png"},
{"code":"IMP","country":"Manx pound","flag":"219-isle-of-man.png"},
{"code":"INR","country":"Indian Rupee","flag":"246-india.png"},
{"code":"IQD","country":"Iraqi Dinar","flag":"020-iraq.png"},
{"code":"IRR","country":"Iranian Rial","flag":"136-iran.png"},
{"code":"ISK","country":"Icelandic Króna","flag":"080-iceland.png"},
{"code":"JEP","country":"Jersey Pound","flag":"245-jersey.png"},
{"code":"JMD","country":"Jamaican Dollar","flag":"037-jamaica.png"},
{"code":"JOD","country":"Jordanian Dinar","flag":"077-jordan.png"},
{"code":"JPY","country":"Japanese Yen","flag":"063-japan.png"},
{"code":"KES","country":"Kenyan Shilling","flag":"067-kenya.png"},
{"code":"KGS","country":"Kyrgystani Som","flag":"152-kyrgyzstan.png"},
{"code":"KHR","country":"Cambodian Riel","flag":"159-cambodia.png"},
{"code":"KMF","country":"Comorian Franc","flag":"029-comoros.png"},
{"code":"KPW","country":"North Korean Won","flag":"030-north-korea.png"},
{"code":"KRW","country":"South Korean Won","flag":"094-south-korea.png"},
{"code":"KWD","country":"Kuwaiti Dinar","flag":"107-kwait.png"},
{"code":"KYD","country":"Cayman Islands Dollar","flag":"051-cayman-islands.png"},
{"code":"KZT","country":"Kazakhstani Tenge","flag":"074-kazakhstan.png"},
{"code":"LAK","country":"Laotian Kip","flag":"112-laos.png"},
{"code":"LBP","country":"Lebanese Pound","flag":"018-lebanon.png"},
{"code":"LKR","country":"Sri Lankan Rupee","flag":"127-sri-lanka.png"},
{"code":"LRD","country":"Liberian Dollar","flag":"169-liberia.png"},
{"code":"LSL","country":"Lesotho Loti","flag":"176-lesotho.png"},
{"code":"LTL","country":"Lithuanian Litas","flag":"064-lithuania.png"},
{"code":"LVL","country":"Latvian Lats","flag":"044-latvia.png"},
{"code":"LYD","country":"Libyan Dinar","flag":"231-libya.png"},
{"code":"MAD","country":"Moroccan Dirham","flag":"166-morocco.png"},
{"code":"MDL","country":"Moldovan Leu","flag":"212-moldova.png"},
{"code":"MGA","country":"Malagasy Ariary","flag":"242-madagascar.png"},
{"code":"MKD","country":"Macedonian Denar","flag":"236-republic-of-macedonia.png"},
{"code":"MMK","country":"Myanma Kyat","flag":"058-myanmar.png"},
{"code":"MNT","country":"Mongolian Tugrik","flag":"258-mongolia.png"},
{"code":"MOP","country":"Macanese Pataca","flag":"167-macao.png"},
{"code":"MRO","country":"Mauritanian Ouguiya","flag":"050-mauritania.png"},
{"code":"MUR","country":"Mauritian Rupee","flag":"001-mauritius.png"},
{"code":"MVR","country":"Maldivian Rufiyaa","flag":"225-maldives.png"},
{"code":"MWK","country":"Malawian Kwacha","flag":"214-malawi.png"},
{"code":"MXN","country":"Mexican Peso","flag":"252-mexico.png"},
{"code":"MYR","country":"Malaysian Ringgit","flag":"118-malasya.png"},
{"code":"MZN","country":"Mozambican Metical","flag":"096-mozambique.png"},
{"code":"NAD","country":"Namibian Dollar","flag":"062-namibia.png"},
{"code":"NGN","country":"Nigerian Naira","flag":"086-nigeria.png"},
{"code":"NIO","country":"Nicaraguan Córdoba","flag":"007-nicaragua.png"},
{"code":"NOK","country":"Norwegian Krone","flag":"143-norway.png"},
{"code":"NPR","country":"Nepalese Rupee","flag":"016-nepal.png"},
{"code":"NZD","country":"New Zealand Dollar","flag":"121-new-zealand.png"},
{"code":"OMR","country":"Omani Rial","flag":"004-oman.png"},
{"code":"PAB","country":"Panamanian Balboa","flag":"106-panama.png"},
{"code":"PEN","country":"Peruvian Nuevo Sol","flag":"188-peru.png"},
{"code":"PGK","country":"Papua New Guinean Kina","flag":"163-papua-new-guinea.png"},
{"code":"PHP","country":"Philippine Peso","flag":"192-philippines.png"},
{"code":"PKR","country":"Pakistani Rupee","flag":"100-pakistan.png"},
{"code":"PLN","country":"Polish Zloty","flag":"211-poland.png"},
{"code":"PYG","country":"Paraguayan Guarani","flag":"041-paraguay.png"},
{"code":"QAR","country":"Qatari Rial","flag":"my-own-qatar.png"},
{"code":"RON","country":"Romanian Leu","flag":"109-romania.png"},
{"code":"RSD","country":"Serbian Dinar","flag":"071-serbia.png"},
{"code":"RUB","country":"Russian Ruble","flag":"248-russia.png"},
{"code":"RWF","country":"Rwandan Franc","flag":"206-rwanda.png"},
{"code":"SAR","country":"Saudi Riyal","flag":"133-saudi-arabia.png"},
{"code":"SBD","country":"Solomon Islands Dollar","flag":"085-solomon-islands.png"},
{"code":"SCR","country":"Seychellois Rupee","flag":"253-seychelles.png"},
{"code":"SDG","country":"Sudanese Pound","flag":"199-sudan.png"},
{"code":"SEK","country":"Swedish Krona","flag":"184-sweden.png"},
{"code":"SGD","country":"Singapore Dollar","flag":"230-singapore.png"},
{"code":"SHP","country":"Saint Helena Pound","flag":"my-own-st-helena.png"},
{"code":"SLL","country":"Sierra Leonean Leone","flag":"092-sierra-leone.png"},
{"code":"SOS","country":"Somali Shilling","flag":"083-somalia.png"},
{"code":"SRD","country":"Surinamese Dollar","flag":"076-suriname.png"},
{"code":"STD","country":"São Tomé and Príncipe Dobra","flag":"012-sao-tome-and-prince.png"},
{"code":"SVC","country":"Salvadoran Colón","flag":"015-el-salvador.png"},
{"code":"SYP","country":"Syrian Pound","flag":"022-syria.png"},
{"code":"SZL","country":"Swazi Lilangeni","flag":"154-swaziland.png"},
{"code":"THB","country":"Thai Baht","flag":"238-thailand.png"},
{"code":"TJS","country":"Tajikistani Somoni","flag":"196-tajikistan.png"},
{"code":"TMT","country":"Turkmenistani Manat","flag":"229-turkmenistan.png"},
{"code":"TND","country":"Tunisian Dinar","flag":"049-tunisia.png"},
{"code":"TOP","country":"Tongan Pa?anga","flag":"191-tonga.png"},
{"code":"TRY","country":"Turkish Lira","flag":"218-turkey.png"},
{"code":"TTD","country":"Trinidad and Tobago Dollar","flag":"181-trinidad-and-tobago.png"},
{"code":"TWD","country":"New Taiwan Dollar","flag":"202-taiwan.png"},
{"code":"TZS","country":"Tanzanian Shilling","flag":"006-tanzania.png"},
{"code":"UAH","country":"Ukrainian Hryvnia","flag":"145-ukraine.png"},
{"code":"UGX","country":"Ugandan Shilling","flag":"009-uganda.png"},
{"code":"USD","country":"United States Dollar","flag":"my-own-united-states.png"},
{"code":"UYU","country":"Uruguayan Peso","flag":"088-uruguay.png"},
{"code":"UZS","country":"Uzbekistan Som","flag":"190-uzbekistn.png"},
{"code":"VEF","country":"Venezuelan Bolívar","flag":"139-venezuela.png"},
{"code":"VND","country":"Vietnamese Dong","flag":"220-vietnam.png"},
{"code":"VUV","country":"Vanuatu Vatu","flag":"187-vanuatu.png"},
{"code":"WST","country":"Samoan Tala","flag":"251-samoa.png"},
{"code":"XAF","country":"CFA Franc BEAC","flag":"no-flag.png"},
{"code":"XAG","country":"Silver (troy ounce)","flag":"no-flag.png"},
{"code":"XAU","country":"Gold (troy ounce)","flag":"no-flag.png"},
{"code":"XCD","country":"East Caribbean Dollar","flag":"no-flag.png"},
{"code":"XDR","country":"Special Drawing Rights","flag":"no-flag.png"},
{"code":"XOF","country":"CFA Franc BCEAO","flag":"no-flag.png"},
{"code":"XPF","country":"CFP Franc","flag":"no-flag.png"},
{"code":"YER","country":"Yemeni Rial","flag":"232-yemen.png"},
{"code":"ZAR","country":"South African Rand","flag":"200-south-africa.png"},
{"code":"ZMK","country":"Zambian Kwacha (pre-2013)","flag":"032-zambia.png"},
{"code":"ZMW","country":"Zambian Kwacha","flag":"032-zambia.png"},
{"code":"ZWL","country":"Zimbabwean Dollar","flag":"011-zimbabwe.png"},

]
