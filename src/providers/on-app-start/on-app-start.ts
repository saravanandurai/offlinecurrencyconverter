import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Rx";
import { Events, Platform } from 'ionic-angular'; 

import { Storage } from '@ionic/storage';
import { ExRatesProvider } from '../ex-rates/ex-rates';
import { SettingsProvider } from '../settings/settings';
import { BackgroundServiceProvider } from '../background-service/background-service'; 
import { Network } from '@ionic-native/network';
import { AdmobProvider } from '../admob/admob';
import * as moment from 'moment';
/*
  Generated class for the OnAppStartProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class OnAppStartProvider {

  
  timeout;
  

  constructor(
              //public http: Http,
                private storage: Storage,
                private exRates: ExRatesProvider,
                private settingsProv: SettingsProvider,
                private backgroundServ: BackgroundServiceProvider,
                private network: Network,
                private events: Events,
                private admobProv: AdmobProvider,
                private platform: Platform,

            ) {
    console.log('Hello OnAppStartProvider Provider');
  }

  onStart(){

//===============================================================================
    console.log("App OnStart");
    this.storage.get("launchCounter").then(val=>{
      if(val > 0){

        let nVal = val + 1;
        this.storage.set("launchCounter", nVal);
        console.log("App Run Post First Launch.");
        this.settingsProv.getSettings().then(settings=>{
          if(settings.ratesFetch == true){
            this.backgroundServ.setBackgroundGetRates();
          } 
        })
        this.exRates.getAndSetRates();

        this.admobProv.showBanner();

       this.timeout =  setTimeout(()=>{
          this.events.publish("runVideoAd");
         this.admobProv.showVideo();
        }, 30000);
        
      }
      else{
        this.storage.set("launchCounter",1);
        console.log("App First Run");        
        this.settingsProv.setSettings({"homeCurr":"USD", "frequentForeignCurr":"EUR", "ratesFetch":true}).then(setval=>{
          this.exRates.getAndSetRates();       
        });
        this.backgroundServ.setBackgroundGetRates();

        this.admobProv.showBanner();
        this.timeout = setTimeout(()=>{
          this.events.publish("runVideoAd");
          this.admobProv.showVideo();          
        }, 60000)
                
      } 
    })

    this.platform.pause.subscribe(()=>{
      console.log("Clearing Timeout");            
      clearTimeout(this.timeout);
    })
    
    this.platform.resume.subscribe(()=>{
      console.log("Resuming App"); 
      
      this.storage.get("videoAd").then(val=>{        
        if(moment(new Date()).diff(moment(val),"minutes") > 6){
          console.log("In If");
          this.timeout = setTimeout(()=>{
            this.events.publish("runVideoAd");
            this.admobProv.showVideo();          
        }, 60000)
        }else{
          console.log("In Else");
          this.timeout = setTimeout(()=>{
            this.events.publish("runVideoAd");
            this.admobProv.showVideo();          
        }, 300000)
        }
      })       
    })
   
    this.platform.registerBackButtonAction(()=>{
      console.log("Register Back Button");
      this.backgroundServ.overRideBackButton();
    })
  }

  getLaunchCounter(): any {
    return this.storage.get("launchCounter");
  }

}
