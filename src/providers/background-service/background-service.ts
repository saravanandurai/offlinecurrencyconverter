import { Injectable, NgZone } from '@angular/core';
//import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';
import { Observable, Subscriber } from 'rxjs';
import  { IntervalObservable } from 'rxjs/observable/IntervalObservable';

import { ExRatesProvider } from '../ex-rates/ex-rates'; 

import { BackgroundMode } from '@ionic-native/background-mode';
/*
  Generated class for the BackgroundServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BackgroundServiceProvider {

  subscriber;
  
  constructor(
              //public http: Http
              private exRates: ExRatesProvider,
              
              private backgroundMode: BackgroundMode,
              private zone: NgZone,

              ) {
    console.log('Hello BackgroundServiceProvider Provider');
  }



  setBackgroundGetRates(){

    
    this.enableBackgroundMode();       
    
    if(this.subscriber == null || this.subscriber == undefined){
      this.subscriber = Observable.interval(3600000)
      //this.subscriber = Observable.interval(30000)
          .subscribe(()=>{
                this.exRates.getAndSetRates();
                console.log("Start Background Rates Fetch.")
          }) 
    }
    
  }

  stopBackgroundGetRates(){
    
    if(this.subscriber != null){
      this.subscriber.unsubscribe();
      this.subscriber = null;     
      console.log("Stopped Background Rates Fetch.");
    }
    this.disableBackgroundMode();
    
  }

  

  enableBackgroundMode(){
    if(!this.backgroundMode.isEnabled()){
      console.log("Enabled Background Mode & Override Back Button");
      this.backgroundMode.setDefaults({"title":"Offline Currency Converter", "text":"Fetching Rates in Background."});
      this.backgroundMode.enable();      
      console.log("Enabled Background Mode Set");      
    }
    
  }

  disableBackgroundMode(){
    if(this.backgroundMode.isEnabled()){
      this.backgroundMode.disable();
      console.log("Disable Background Mode");
    }
    
  }

  getIsScreenOff():Promise<boolean>{
    return new Promise(resolve=>{
      let stat = this.backgroundMode.isScreenOff();
      console.log("Stat: " + stat);
      resolve(stat);
    })
    
  }

  overRideBackButton(){
    this.backgroundMode.overrideBackButton();
  }
}
