import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig, AdMobFreeRewardVideoConfig } from '@ionic-native/admob-free';
import { BackgroundServiceProvider } from "../background-service/background-service";
/*
  Generated class for the AdmobProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AdmobProvider {

  constructor(
                private admobFree: AdMobFree,
                private storage: Storage,
                private backgroundServ: BackgroundServiceProvider,
              ) {
    console.log('Hello AdmobProvider Provider');
  }

  showBanner(){
    let bannerConfig: AdMobFreeBannerConfig = {
      isTesting: false,
      autoShow: true,
      id: "ca-app-pub-7415907348091573/9763476782"
    }

    
    this.admobFree.banner.config(bannerConfig);
    
    this.admobFree.banner.prepare().then(()=>{
      console.log("MyAdd Banner");
    }).catch((e)=>{
      console.log("Error: " + e);
    })
    

  }

  showInterstitial(){
    let interstitialConfig: AdMobFreeInterstitialConfig = {
      isTesting: false,
      autoShow: true,
      id: "ca-app-pub-7415907348091573/3034239296"
    }

    this.admobFree.interstitial.config(interstitialConfig);
    
    this.admobFree.interstitial.prepare().then(()=>{
      console.log("MyAdd Interstitial");
    }).catch((e)=>{
      console.log("Error: " + e);
    })
    

  }

  showVideo(){
    let rewardVideoConfig: AdMobFreeRewardVideoConfig = {
      isTesting: false,
      autoShow: true,
      id: "ca-app-pub-7415907348091573/6590340924"
    }

    this.admobFree.rewardVideo.config(rewardVideoConfig);      
    this.admobFree.rewardVideo.prepare().then(()=>{
      console.log("MyAdd RewardVideo");
      this.storage.set("videoAd", moment(new Date()).toISOString());
    }).catch((e)=>{
      console.log("Error: " + e);
    })
  }

  

  

}
