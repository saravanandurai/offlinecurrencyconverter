import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Events } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { RATES } from '../../data/rates-data';
import { Network } from '@ionic-native/network';
import * as moment from 'moment';
/*
  Generated class for the ExRateProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ExRatesProvider {

  constructor(
            public http: Http,
            private storage: Storage,
            private events: Events,
            private network: Network,
          
          ) {
    console.log('Hello ExRateProvider Provider');
  }


  
  getRates(): Promise<any>{
    return new Promise(resolve=>{
      this.storage.get("rates").then(val=>{
        if(val == null){
          resolve("noData");
        }else{
          resolve(val);
        }
        
      })      
    })
    
    
  }

  getAndSetRates(){
    this.getDataFromServer().subscribe(data=>{
      if(data!=null && data!= undefined && data != ""){
        this.setRates(data);        
      }
    }, err=>{
      console.log("Ooops something went wrong!");
      this.events.publish("networkError");                 
      this.network.onConnect().subscribe(()=>{
        console.log("Subscribed to Network OnConnect.");

        this.getRates().then(rRates=>{
          if(rRates == "noData"){
            this.getAndSetRates(); 
          }else{
            this.getLastUpdate().then(lstUpdData=>{
              if(moment(new Date()).diff(moment(lstUpdData),'days') > 1){
                this.getAndSetRates();
              }
            })
          }
        })
                   
      })
    })
  }

  setRates(rates: any): Promise<any>{
    return new Promise(resolve=>{
      this.getRates().then(oRates=>{        
        this.storage.set("rates", rates).then(val=>{
          console.log("Set Rates Data.");
          this.setLastupdate();
          if(oRates == "noData"){
            this.events.publish("ratesSet");
          }else{
            this.events.publish("ratesUpdated");
          }          
          resolve(val);
        })
        

          

      
      })     
             
      })     
    }


  getDataFromServer():Observable<any>{
    return this.http.get('http://139.59.34.115:3579/rates').map((res:Response)=>res.json());   
  }

    
  setLastupdate(){
    this.storage.set("lastUpdate",moment(new Date()).toISOString());
  }

  getLastUpdate():Promise<any>{
    return new Promise(resolve=>{
      this.storage.get("lastUpdate").then(val=>{
        resolve(val);
      })
    })
    
  }
}
