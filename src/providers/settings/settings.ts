import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
//import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Settings } from '../../models/settings';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SettingsProvider {
  

  constructor(
              //public http: Http
                private storage: Storage,
                private events: Events,
              
              ) {
    console.log('Hello SettingsProvider Provider');
  }

  setSettings(settings: Settings): Promise<any>{
    return new Promise(resolve=>{     
      this.storage.set("settings",settings).then(val=>{
        this.events.publish("settingsSet");
        console.log("Settings Set successfully.");
        resolve(val);
      })
    })    
  }

  getSettings():Promise<any>{
    return new Promise(resolve=>{
      this.storage.get("settings").then(va=>{
        console.log("Get Settings");
        if(va == null){
          resolve("noData");
        }else{
        resolve(va);
        }
      })
    })

  }
}

