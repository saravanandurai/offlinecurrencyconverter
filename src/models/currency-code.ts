export interface CurrencyCode {
    code: string;
    country: string;
    flag: string;
}