export interface Settings {
    homeCurr: string;
    frequentForeignCurr: string;
    ratesFetch: boolean;
}